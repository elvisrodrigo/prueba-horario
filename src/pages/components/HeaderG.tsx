import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
}

/* function createNavElements(name: string, ref: string) {
  return { name, ref }
}

const navItems = [
  createNavElements('Home', '/'),
  createNavElements('About', '/tour/About'),
  createNavElements('Explorer', 'tour/Explorer'),
  createNavElements('Gallery', '/tour/Gallery'),
  createNavElements('Contact', '/tour/Contact'),
] */
const drawerWidth = 240;
/* const navItems = ['Home', 'About', 'Explorer','Gallery','Contact']; */

export default function DrawerAppBar(props: Props) {
  const { window } = props;
  /* const [click2, setClick2] = React.useState(); */
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };


  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 3 }}>
        <div className="tourrr">
          TE QUIERO 
        </div>
      </Typography>
      <Divider />
      <List>
        {/* {navItems.map((item) => (
          <ListItem key={item} disablePadding> */}
            <ListItemButton sx={{ textAlign: '',textDecoration:'none' }}>
              {/* <ListItemText primary={item} /> */}
              <div className="navbara" style={{display:"flex",flexDirection:"column"}}>
             
                
              
                  <a  href='/tour/Explorer'>
                    MERY
                  </a>
                  <a href='/tour/Gallery'>
                    POETRY
                  </a>
                  <a href='/tour/Contact' >
                    FRODRY
                  </a>
                  <a href='/tour/About'>
                    ANIMACION
                  </a>
                  </div>
            </ListItemButton>
         {/*  </ListItem>
        ))} */}
      </List>
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <>
    <header>
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar style={{backgroundColor:"Salmon",paddingLeft:"3em",paddingRight:"3em"}}  component="nav">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
          >
            
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            {/* {navItems.map((item) => (
              <Button key={item} sx={{ color: '#fff' }}>
                <div className="navegation">
                <div className="navegation-items">
                {item}
                </div>
              </div>
              
              </Button>
            ))} */}
             <div style={{display:"flex",justifyContent:"center"}}>
             <div className="navegation">
                <div className="navegation-items">
                  
                <a href='/#'>
                    Home
                  </a>
                  <a href='/tour/Explorer'>
                    MERY
                  </a>
                  <a href='/tour/Gallery'>
                    GALLERY
                  </a>
                  <a href='/tour/Contact' >
                    FRODRY
                  </a>
                  <a href='/tour/About' >
                    About
                  </a>
                </div>
              </div>
             
             </div>
              
          </Box>
        </Toolbar>
       
      </AppBar>
      <Box component="nav">
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
      </Box>
      <Box component="main" sx={{ p: 3 }}>
        <Toolbar />
       
      </Box>
    </Box>
    </header>
    </>
  );
}

