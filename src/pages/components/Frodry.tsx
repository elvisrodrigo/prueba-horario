import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

export default function Frodry(props: any) {
  const [expanded, setExpanded] = React.useState<string | false>(false);

  const handleChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };

  return (
    <div>
      <Accordion style={{backgroundColor:"LightCoral"}} expanded={expanded === 'panel11'} onChange={handleChange('panel11')}>
        <AccordionSummary
        style={{}}
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography sx={{ width: '33%', flexShrink: 0 }}>
          <h4 >{props.c}</h4>
          </Typography>
          <Typography sx={{ color: 'text.secondary' }}>
          <h4 >{props.c1}</h4>
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <h5 >{props.d}</h5>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{backgroundColor:"IndianRed"}} expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography sx={{ width: '33%', flexShrink: 0 }}> 
          <h4 >{props.e}</h4>
          </Typography>
          <Typography sx={{ color: 'text.secondary' }}>
          <h4 >{props.e1}</h4>
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <h5 >{props.f}</h5>
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
