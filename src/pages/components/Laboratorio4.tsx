import { Grid } from "@mui/material"
import { Item } from "./ItemG"
import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <section  className="formulario" style={{paddingTop:"7em",paddingBottom:"7em"}}>
              <div className="color-form" style={{width:"80%" ,margin:"auto"}}>
              <div >
              
              <Grid container spacing={2}>
              
              <Grid /* style={{marginTop:"-2em",marginBottom:"-2em"}} */ className="color-form1" item xs={6}>
               <div className="color-fo">
               <Item>
                <div style={{display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column",paddingRight:"7em",color:"white"}}>
                <h4 className="text2" >Carrera:  </h4>
                <h4 className="text2" style={{textTransform:"uppercase",padding:"2em"}} >ingecnieria de sistemas</h4>
                <h4 className="text2" >Materia: </h4>
                <h4 className="text2" style={{textTransform:"uppercase",padding:"2em"}} > tecnologias emergentes</h4>
                <h4 className="text2" >Proyecto: </h4>
                <h4 className="text2" style={{textTransform:"uppercase",padding:"2em"}}>sistema web de un tour</h4>
                </div>
                                   <div style={{paddingLeft:"3em"}}>
                                   <Card sx={{ maxWidth: 280 }}>
                        <CardActions disableSpacing>
                            MORE
                            {/* <IconButton aria-label="add to favorites">
                            <FavoriteIcon />
                            </IconButton>
                            <IconButton aria-label="share">
                            <ShareIcon />
                            </IconButton> */}
                            <ExpandMore
                            expand={expanded}
                            onClick={handleExpandClick}
                            aria-expanded={expanded}
                            aria-label="show more"
                            >
                            <ExpandMoreIcon />
                            </ExpandMore>
                        </CardActions>
                        <Collapse in={expanded} timeout="auto" unmountOnExit>
                            <CardContent>
                            <Typography paragraph>Contact</Typography>
                            <Typography paragraph>
                                Email: ec7902812@gmail.com
                            </Typography>
                            <Typography paragraph>
                                Number: 68409023            
                            </Typography>
                            </CardContent>
                        </Collapse>
                        </Card>
                                   </div>
               </Item>
               </div>
              </Grid>
             
              <Grid item xs={6}>
                <Item>
                <div style={{display:"flex"}}>
                <div style={{display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column"}}>
                  <div style={{display:"flex",justifyContent:"center",alignItems:"center"}}>
                  <h1 className="texto1" style={{fontWeight:"bold",fontSize:"2.2em"}}>ELVIS RODRIGO</h1>
                  </div>
                  <h3 className="texto1">CONDORI VASQUEZ</h3>
                       
                      </div>
                      <div style={{width:"100%"}}>
                      <img className="foto1" style={{width:"100%",padding:"5em" }} src="/img/elvis.jpeg" alt="" />
                      </div>
                </div>
                </Item>
              </Grid>
              
            </Grid>
              </div>
              </div>
          </section>
  );
}
