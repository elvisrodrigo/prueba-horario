import Grid from "@mui/material/Grid";
import { Item } from "../components/ItemG";
import TextField from "@mui/material/TextField";
import {  Link} from "@mui/material";
import EmailIcon from "@mui/icons-material/Email";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
export default function Footer(props: any) {
    return (
        <>
            <footer
            className="footer-six"
            style={{ paddingBottom:"3em", marginTop: "2em" }}
          >
            <div className="container " style={{display:"flex"}}>
              <Grid container spacing={1}>
                <Grid item xs={4}>
                  <Item>
                    <div data-aos="fade-up" style={{ width: "70%", float: "right" }}>
                      <h1
                        className=""
                        style={{ paddingTop: "2em", paddingBottom: "1em",color:"black" }}
                      >
                        Music
                      </h1>
                      <p style={{ paddingBottom: "2em",color:"black" }}>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Nesciunt asperiores obcaecati aliquam necessitatibus
                        aperiam vel illum?
                      </p>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <TextField
                          style={{ color: "red" }}
                          id="outlined-basic"
                          label="Sutmit email"
                          variant="outlined"
                        />
                      </div>
                    </div>
                  </Item>
                </Grid>
                <Grid item xs={1}>
                  <Item>
                    <div data-aos="fade-up" style={{ float: "right" }}>
                      <h1 style={{color:"black"}}>About</h1>
                      <p style={{ paddingTop: "1em",color:"black" }}>About us</p>
                      <p style={{ paddingTop: "1em",color:"black" }}>Service'</p>
                      <p style={{ paddingTop: "1em",color:"black" }}>Our Story</p>
                      <p style={{ paddingTop: "1em",color:"black" }}>Success</p>
                      <p style={{ paddingTop: "1em",color:"black" }}>Support</p>
                    </div>
                  </Item>
                </Grid>
                <Grid item xs={2}>
                  <Item>
                    <div data-aos="fade-up" style={{ float: "right" }}>
                      <h1 style={{color:"black"}}>Service</h1>
                      <p style={{ paddingTop: "1em",color:"black" }}>Development</p>
                      <p style={{ paddingTop: "1em",color:"black" }}>Maintanance</p>
                      <p style={{ paddingTop: "1em",color:"black" }}>Consultancy</p>
                      <p style={{ paddingTop: "1em",color:"black", paddingBottom: "2em" }}>
                        Design
                      </p>
                    </div>
                  </Item>
                </Grid>
                <Grid item xs={1}>
                  <Item></Item>
                </Grid>
                <Grid item xs={4}>
                  <Item>
                    <h1 style={{color:"black"}} data-aos="fade-up">Get in Touch</h1>
                    <div data-aos="fade-up"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        paddingTop: "1em",
                      }}
                    >
                      <Link href="#" underline="hover">
                        <LocationOnIcon sx={{ fontSize: 20, color: "black" }} />
                      </Link>
                      <Link
                        style={{ color: "black" }}
                        href="#"
                        underline="none"
                      >
                        {" "}
                        {"22/1 Bardeshi, Amin Barzar Dhaka 1348"}
                      </Link>
                    </div>
                    <div data-aos="fade-up"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        paddingTop: "1em",
                      }}
                    >
                      <Link href="#" underline="hover">
                        <EmailIcon sx={{ fontSize: 20, color: "black" }} />
                      </Link>
                      <Link
                        style={{ color: "black" }}
                        href="#"
                        underline="none"
                      >
                        {" "}
                        {"xuwelkhan@gmail.com"}
                      </Link>
                    </div>
                    <div data-aos="fade-up"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        paddingTop: "1em",
                        paddingBottom: "2.5em",
                      }}
                    >
                      <Link href="#" underline="hover">
                        <LocalPhoneIcon sx={{ fontSize: 20, color: "black" }} />
                      </Link>
                      <Link
                        style={{ color: "black" }}
                        href="#"
                        underline="none"
                      >
                        {" "}
                        {"+88 01679 252595"}
                      </Link>
                    </div>
                  </Item>
                </Grid>
              </Grid>
            </div>
          </footer>
        </>
    )
}
