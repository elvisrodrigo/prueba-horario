import  HeaderG  from "@/pages/components/HeaderG"
import  Footer  from "@/pages/components/Footer"
import * as React from 'react';
import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import Frodry from "@/pages/components/Frodry";
import MuiAccordionSummary, {
  AccordionSummaryProps,
} from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';

const Accordion = styled((props: AccordionProps) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0,
  },
  '&:before': {
    display: 'none',
  },
}));

const AccordionSummary = styled((props: AccordionSummaryProps) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === 'dark'
      ? 'rgba(255, 255, 255, .05)'
      : 'rgba(0, 0, 0, .03)',
  flexDirection: 'row-reverse',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-content': {
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));


 
export default function ContactRouter(){
    
      (panel: string) => (event: React.SyntheticEvent, newExpanded: boolean) => {
        setExpanded(newExpanded ? panel : false);
      };
      const [expanded, setExpanded] = React.useState<string | false>(false);

  const handleChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };
    return(
        <>
        <HeaderG/>
        <div style={{paddingTop:"5em"}}></div>
        <div>
      <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
          <Typography>No da</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum dolor
            sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
            sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
        <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
          <Typography>MONDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-544 G1" d="INVESTIGACION OPERATIVA II 2do amb. 6" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-443 G1" d="INVESTIGACION OPERATIVA I " e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-523 G1" d="REDES I 2do amb. 2" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
        <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
          <Typography>TUESDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-544 G1" d="INVESTIGACION OPERATIVA II 2do amb. 6" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-443 G1" d="INVESTIGACION OPERATIVA I " e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-523 G1" d="REDES I 2do amb. 2" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
        <AccordionSummary aria-controls="panel4d-content" id="panel4d-header">
          <Typography>WESNESDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-544 G1" d="INVESTIGACION OPERATIVA II 2do amb. 6" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-443 G1" d="INVESTIGACION OPERATIVA I " e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-523 G1" d="REDES I 2do amb. 2" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel5'} onChange={handleChange('panel5')}>
        <AccordionSummary aria-controls="panel5d-content" id="panel5d-header">
          <Typography>THURSDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-544 G1" d="INVESTIGACION OPERATIVA II 2do amb. 6" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-443 G1" d="INVESTIGACION OPERATIVA I " e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-523 G1" d="REDES I 2do amb. 2" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel6'} onChange={handleChange('panel6')}>
        <AccordionSummary aria-controls="panel6d-content" id="panel6d-header">
          <Typography>FRIDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-544 G1" d="INVESTIGACION OPERATIVA II 2do amb. 6" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-443 G1" d="INVESTIGACION OPERATIVA I " e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-523 G1" d="REDES I 2do amb. 2" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel7'} onChange={handleChange('panel7')}>
        <AccordionSummary aria-controls="panel7d-content" id="panel7d-header">
          <Typography>SATURDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-544 G1" d="INVESTIGACION OPERATIVA II 2do amb. 6" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-443 G1" d="INVESTIGACION OPERATIVA I " e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-523 G1" d="REDES I 2do amb. 2" e="08:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 6" />
          
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
        
        </>
    )
}
